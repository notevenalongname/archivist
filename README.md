# Archivist

A Discord archiving utility.

### Usage

```
archivist.py [-h] [--email EMAIL] [--password PASSWORD]
             [-v | -q | --progress] output
```

### Dependencies

Archivist requires the `discord.py` package, and at least Python version
3.4.2. You can install the necessary dependencies via 
`pip install discord.py`.

### Output

The output is a SQLite database with the following schema:

```sql
CREATE TABLE users    (id INTEGER PRIMARY KEY,
                       name TEXT,
                       discriminator INTEGER);
CREATE TABLE servers  (id INTEGER PRIMARY KEY,
                       name TEXT);
CREATE TABLE channels (id INTEGER PRIMARY KEY,
                       server INTEGER,
                       name TEXT,
                       FOREIGN KEY(server) REFERENCES servers(id));
CREATE TABLE messages (id INTEGER PRIMARY KEY,
                       author INTEGER,
                       timestamp INTEGER,
                       edited_timestamp INTEGER,
                       content TEXT,
                       clean_content TEXT,
                       channel INTEGER,
                       FOREIGN KEY(author) REFERENCES users(id),
                       FOREIGN KEY(channel) REFERENCES channels(id));
```

### Troubleshooting

On Windows, you may encounter Unicode errors in the output. Try
specifying the `-q` flag to suppress as much output as possible
