#!/usr/bin/python
# -*- coding: utf-8 -*-

# Dependencies:
#  - discord.py

import argparse
import asyncio
import discord
import getpass
import itertools
import sqlite3

if __name__ != "__main__":
    raise ImportError("archivist.py must be executed, not imported.")

## Logging

QUIET = 0
DEFAULT = 1
DETAILED = 2
VERBOSE = 3
EXCESSIVE = 4

class Logger:
    def __init__(self):
        self.loglevel = DEFAULT
    def load_verbosity(self, verbose_count, is_progress, is_quiet):
        if is_quiet:
            self.loglevel = QUIET
        elif is_progress:
            self.loglevel = DETAILED
        else:
            self.loglevel = DEFAULT + verbose_count if verbose_count else 0
    def __call__(self, level, *args, **kwargs):
        if level <= self.loglevel:
            print(*args, **kwargs)

log = Logger()

## Utility functions

def select(options, message, key=lambda x: x, prompt="Enter a selection (default=all): ", error_message="Invalid selection."):
    """Reads and parses a selection string.
    Empty values and the string 'all' select all items
    Individual items can be selected via their numbers or excluded via
    ^A. Ranges can be specified as A-B (and ^A-B). If the first piece is
    an exclusion, all items are implicitly included at first."""
    
    ## Print message and options
    
    log(QUIET, message)
    for index, option in enumerate(options, start=1):
        log(QUIET, "  {:>4}  {}".format(index, key(option)))
    
    ## Loop on error
    
    while True:    
        ## Read selections
        selection = input(prompt).strip().lower()
        
        try:
            ## Exclude 'all' and ''
            if not selection or selection == "all":
                return options
            
            ## Parse individual items
            
            initial = (selection[0] == "^")
            bitmap = [initial] * len(options)
            
            for item in selection.split():
                included = (item[0] != "^")
                if not included:
                    item = item[1:]
                if "-" in item:
                    ## Parse a range
                    start, end = item.split("-")
                    start, end = int(start) - 1, int(end) - 1
                    if start > end:
                        start, end = end, start
                    for i in range(max(start, 0), min(end + 1, len(bitmap)), 1):
                        bitmap[i] = included
                else:
                    ## Parse a single number
                    bitmap[int(item) - 1] = included
                    
            return list(itertools.compress(options, bitmap))
        except:
            ## Error
            log(QUIET, error_message)
            
## Tuning

class tuning:
    @staticmethod
    def prohibited(value):
        raise KeyError("Cannot tune the specified key")
        
    ## Tuneable parameters
    limit = 100 # Number of messages fetched at a time
    
    ## Types used for conversion
    types = {"limit":      int,
             "types":      prohibited,
             "prohibited": prohibited}
            
## Parse arguments

parser = argparse.ArgumentParser(description="An archiving utility for Discord chats.")
# Positional arguments
parser.add_argument("output",          help="The file to write the archived data to.")
# Optional arguments
parser.add_argument("--email",         help="The account to be used to sign into Discord.")
parser.add_argument("--password",      help="The account password. If you use this flag, beware that it will be visible in process listings.")
parser.add_argument("--channels",      help="Select individual channels to archive, rather than simply archiving all available channels in the selected servers.", action="store_true")
# Output arguments
output = parser.add_mutually_exclusive_group()
output.add_argument("-v", "--verbose", help="Increases verbosity. You may specify this option multiple times.", action="count")
output.add_argument("--progress",      help="Displays progress in each channel. Implies -v.", action="store_true")
output.add_argument("-q", "--quiet",   help="Silences output.", action="store_true")
# Tuning arguments
parser.add_argument("--tune",          help=argparse.SUPPRESS, nargs=2, metavar=("KEY", "VALUE"), action="append")

args = parser.parse_args()

log.load_verbosity(args.verbose, args.progress, args.quiet)
for key, value in args.tune:
    setattr(tuning, key, tuning.types[key](value))

## Fetch additional data (e.g. unspecified login information)

if not args.email or not args.password:
    log(QUIET, "Please sign in with your Discord account information:")
    if not args.email:
        args.email = input("email: ")
    if not args.password:
        args.password = getpass.getpass("password: ")
        
## Initialize Discord API

client = discord.Client()
            
## On login, this function is called

@client.event
@asyncio.coroutine
def on_ready():
    log(DETAILED, "Authenticated as {}#{}".format(client.user.name, client.user.discriminator))
    
    ## Create the output database
    
    database = sqlite3.connect(args.output)
    def trace_callback(*args, **kwargs):
        if len(args) > 0 and len(args[0].split()) > 1:
            # This only allows multi-word statements to print (ie. not BEGIN and COMMIT)
            log(EXCESSIVE, "           ", *args, **kwargs) # One space less indented than desired, because the comma adds another one
    database.set_trace_callback(trace_callback)
    cursor = database.cursor()
    
    cursor.execute("""CREATE TABLE IF NOT EXISTS users    (id INTEGER PRIMARY KEY, name TEXT, discriminator INTEGER)""")
    cursor.execute("""CREATE TABLE IF NOT EXISTS servers  (id INTEGER PRIMARY KEY, name TEXT)""")
    cursor.execute("""CREATE TABLE IF NOT EXISTS channels (id INTEGER PRIMARY KEY, server INTEGER, name TEXT,
                                                           FOREIGN KEY(server) REFERENCES servers(id))""")
    cursor.execute("""CREATE TABLE IF NOT EXISTS messages (id INTEGER PRIMARY KEY, author INTEGER, timestamp INTEGER,
                                                           edited_timestamp INTEGER, content TEXT, clean_content TEXT,
                                                           channel INTEGER,
                                                           FOREIGN KEY(author) REFERENCES users(id),
                                                           FOREIGN KEY(channel) REFERENCES channels(id))""")
    database.commit()
                      
    
    ## Grab the list of servers the user is a member of, and let the user select some to archive
    
    servers = sorted(client.servers, key=lambda server: server.id)
    servers = select(servers, "Select servers to archive messages from:", lambda server: server.name)
    
    ## On each server, grab the target channels (by default, all text channels, unless --channels is specified)
    
    total = 0
    log(DEFAULT)
    
    for server in servers:
        log(DEFAULT, "Archiving '{}'".format(server.name))

        ## Filter the channels by type and by whether we can read the chat messages
        
        channels = [channel for channel in server.channels if channel.type == discord.ChannelType.text \
                                                          and channel.permissions_for(server.me).read_messages
                                                          and channel.permissions_for(server.me).read_message_history]
        
        ## If configured, let the user select channels
        
        if args.channels:
            channels = select(channels, "Select channels to archive on '{}'".format(server.name), lambda channel: "#" + channel.name)
        
        ## If the number of selected channels is greater than zero, add the server to the database
        
        if len(channels) > 0:
            cursor.execute("""INSERT OR IGNORE INTO servers VALUES (?, ?)""", (server.id, server.name))
            database.commit()
        
        ## Iterate over the channels and start creating the archive
        
        for channel in channels:
            log(DEFAULT, "    Archiving #{}".format(channel.name))
            
            ## Grab an initial list of messages into the buffer
            
            message_buffer_generator = yield from client.logs_from(channel, limit=tuning.limit)
            message_buffer = list(message_buffer_generator)
            written = 0
            
            ## If there is at least one archivable message, add the channel to the database
            
            if len(message_buffer) > 0:
                cursor.execute("""INSERT OR IGNORE INTO channels VALUES (?, ?, ?)""", (channel.id, server.id, channel.name))
                database.commit()
            
            ## Repeatedly load and write messages
            
            while len(message_buffer) > 0:
                written += len(message_buffer)
                log(VERBOSE, "        Loaded {} messages.".format(len(message_buffer)))
                if args.progress:
                    log(DETAILED, "        Loaded {} messages.".format(written), end="\r")
                
                ## Write the buffer into the database
                
                for message in message_buffer:
                    cursor.execute("""INSERT OR IGNORE INTO users VALUES (?, ?, ?)""", (message.author.id, message.author.name, message.author.discriminator))
                    cursor.execute("""INSERT OR IGNORE INTO messages VALUES (?, ?, ?, ?, ?, ?, ?)""", 
                                   (message.id, message.author.id, message.timestamp.timestamp(),
                                    message.edited_timestamp.timestamp() if message.edited_timestamp else None,
                                    message.content, message.clean_content, channel.id))
                
                database.commit()
                
                ## Refill the buffer with older data
                
                message_buffer_generator = yield from client.logs_from(channel, limit=tuning.limit, before=message_buffer[-1])
                message_buffer = list(message_buffer_generator)
                
            if args.progress:
                log(DETAILED)
            log(DETAILED, "        Archived {} messages in channel #{} on server '{}'".format(written, channel.name, server.name))
            total += written
        
        log(DEFAULT)
    
    ## All servers are archived, log out and exit
    
    log(DEFAULT, "Archived {} messages".format(total))
    database.close()
    yield from client.logout()

## Log in and run with user information

log(DETAILED, "Signing in...")
client.run(args.email, args.password)
